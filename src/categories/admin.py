# -*- coding: utf8 -*-

from django.contrib import admin

from src.categories.models import Category


class CategoryAdmin(admin.ModelAdmin):
    search_fields = ('name',)

admin.site.register(Category, CategoryAdmin)

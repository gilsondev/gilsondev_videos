# -*- coding: utf8 -*-

from django.test import TestCase

from categories.models import Category


class CategoryModelTest(TestCase):

    def test_insert_category(self):
        """The category was inserted correctly?"""
        category = Category.objects.create(
            name="Python",
            slug="python",
            thumbnail="/media/categories/python.png"
        )

        self.assertTrue(category.id)

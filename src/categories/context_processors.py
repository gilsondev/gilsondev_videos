# -*- coding: utf8 -*-

from src.categories.models import Category


def list_categories(request):
    """List the categories"""
    categories = Category.objects.all()

    categories_actives = []
    for category in categories:
        if category.videos.all():
            categories_actives.append(category)

    return {'categories': categories_actives}

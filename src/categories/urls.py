# -*- coding: utf8 -*-

from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('src.categories.views',
    # List
    url(r'categorias/$', 'list', name='list'),

    # Details
    url(r'categoria/(?P<slug>[-\w]+)/$', 'details', name='details'),
)

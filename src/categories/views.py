# -*- coding: utf8 -*-

from django.shortcuts import render, get_object_or_404

from src.categories.models import Category


def list(request):
    categories = Category.objects.all()

    return render(request, 'categories/categories_list.html',
        {'categories': categories})


def details(request, slug):
    category = get_object_or_404(Category, slug=slug)

    return render(request, 'categories/categories_details.html',
        {'tutorials': category.videos.all(), 'category': category})

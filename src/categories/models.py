# -*- coding: utf8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from filebrowser.fields import FileBrowseField


class Category(models.Model):
    name = models.CharField(_(u'Nome da Categoria'), max_length=50)
    slug = models.SlugField(max_length=100)
    thumbnail = FileBrowseField(_(u'Thumbnail'), max_length=200, directory="categories")

    class Meta:
        verbose_name = u'Categoria'
        verbose_name_plural = u'Categorias'
        db_table = 'categories'

    def __unicode__(self):
        return self.name

# -*- coding: utf8 -*-

from django.contrib import admin

from src.tutorials.models import Tutorial


class TutorialAdmin(admin.ModelAdmin):
    search_fields = ('title', 'resume')
    list_display = ('title', 'video', 'duration', 'created_at',)
    prepopulated_fields = {'slug': ('title',)}

admin.site.register(Tutorial, TutorialAdmin)

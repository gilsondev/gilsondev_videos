# -*- coding: utf8 -*-

from django.db import models
from django.utils.translation import ugettext as _

from tagging.fields import TagField
from tagging.models import Tag

from src.categories.models import Category
from filebrowser.fields import FileBrowseField


class Tutorial(models.Model):
    category = models.ForeignKey(Category, related_name='videos')
    title = models.CharField(_(u'Título'), max_length=150)
    slug = models.SlugField(max_length=80)
    resume = models.CharField(_(u'Resumo do Vídeo'), max_length=167, blank=True)
    video = models.URLField(_(u'URL do Vídeo'), verify_exists=False,
        max_length=200)
    duration = models.CharField(_(u'Duração do Vídeo'), max_length=20)
    thumbnail = FileBrowseField(_(u'Thumbnail'), max_length=200,
        directory="videos")
    tags = TagField(_(u'Tags do Vídeo'), help_text=_(u'Delimite cada tag por um espaço'))
    created_at = models.DateField(auto_now_add=True)

    class Meta:
        verbose_name = u'Tutorial'
        verbose_name_plural = u'Tutoriais'
        db_table = 'tutorials'
        ordering = ['-created_at']

    def __unicode__(self):
        return self.title

    def _get_tags(self):
        return Tag.objects.get_for_object(self)

    tags_list = property(_get_tags)

    @models.permalink
    def get_absolute_url(self):
        return ('tutorials.views.details', (), {'slug': self.slug})

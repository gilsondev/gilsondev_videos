# -*- coding: utf8 -*-

from django.shortcuts import render, get_object_or_404

from src.tutorials.models import Tutorial

from tagging.models import TaggedItem, Tag


def list(request):
    """Action witch list all tutorials"""
    tutorials = Tutorial.objects.all()
    return render(request, 'tutorials/tutorials_list.html',
        {'tutorials': tutorials})


def list_by_tags(request, tag):
    """List all tutorials by tags"""
    tag = Tag.objects.get(name=tag)
    tutorials = TaggedItem.objects.get_by_model(Tutorial, tag)

    return render(request, 'tutorials/tutorials_list.html',
        {'tutorials': tutorials})


def details(request, slug):
    """Search informations about tutorial by slug"""
    tutorial = get_object_or_404(Tutorial, slug=slug)

    return render(request, 'tutorials/tutorials_details.html',
        {'tutorial': tutorial})

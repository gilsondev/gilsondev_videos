# -*- coding: utf8 -*-

from datetime import datetime

from django.test import TestCase

from categories.models import Category
from tutorials.models import Tutorial


class TutorialsModelTest(TestCase):

    fixtures = ['categories.json']

    def setUp(self):
        self.category = Category.objects.get(pk=1)

    def test_insert_tutorial(self):
        tutorial = Tutorial.objects.create(
            category=self.category,
            title="Virtualenv e Django",
            slug="virtualenv-e-django",
            resume="Nessa video aula voce vai aprender a usar o virtualenv",
            video="http://vimeo.com/22919392",
            duration="16:25",
            thumbnail="/media/videos/virtualenv_web2py.png",
            tags="virtualenv django python",
            created_at=datetime.now()
        )

        self.assertTrue(tutorial.id)

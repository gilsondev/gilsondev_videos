from django.conf.urls.defaults import patterns, url

urlpatterns = patterns('src.tutorials.views',
    # List
    url(r'^$', 'list', name='list'),

    # List by tags
    url(r'^tutoriais/tag/(?P<tag>[-\w]+)/$', 'list_by_tags', name='list_by_tags'),

    # Details
    url(r'^tutoriais/(?P<slug>[-\w]+)/$', 'details', name='details'),
)

# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding field 'Tutorial.category'
        db.add_column('tutorials', 'category', self.gf('django.db.models.fields.related.ForeignKey')(default=1, related_name='videos', to=orm['categories.Category']), keep_default=False)


    def backwards(self, orm):
        
        # Deleting field 'Tutorial.category'
        db.delete_column('tutorials', 'category_id')


    models = {
        'categories.category': {
            'Meta': {'object_name': 'Category', 'db_table': "'categories'"},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'}),
            'thumbnail': ('filebrowser.fields.FileBrowseField', [], {'max_length': '200'})
        },
        'tutorials.tutorial': {
            'Meta': {'ordering': "['-created_at']", 'object_name': 'Tutorial', 'db_table': "'tutorials'"},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'videos'", 'to': "orm['categories.Category']"}),
            'created_at': ('django.db.models.fields.DateField', [], {'auto_now': 'True', 'auto_now_add': 'True', 'blank': 'True'}),
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'resume': ('django.db.models.fields.CharField', [], {'max_length': '167', 'blank': 'True'}),
            'slug': ('django.db.models.fields.SlugField', [], {'max_length': '80', 'db_index': 'True'}),
            'thumbnail': ('filebrowser.fields.FileBrowseField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'video': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['tutorials']

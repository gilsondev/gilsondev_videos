# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Changing field 'Tutorial.resume'
        db.alter_column('tutorials', 'resume', self.gf('django.db.models.fields.CharField')(max_length=167))


    def backwards(self, orm):
        
        # Changing field 'Tutorial.resume'
        db.alter_column('tutorials', 'resume', self.gf('django.db.models.fields.TextField')())


    models = {
        'tutorials.tutorial': {
            'Meta': {'object_name': 'Tutorial', 'db_table': "'tutorials'"},
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'resume': ('django.db.models.fields.CharField', [], {'max_length': '167', 'blank': 'True'}),
            'thumbnail': ('filebrowser.fields.FileBrowseField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'video': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['tutorials']

# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):

    def forwards(self, orm):
        
        # Adding model 'Tutorial'
        db.create_table('tutorials', (
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=150)),
            ('resume', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('video', self.gf('django.db.models.fields.URLField')(max_length=200)),
            ('duration', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('thumbnail', self.gf('filebrowser.fields.FileBrowseField')(max_length=200)),
        ))
        db.send_create_signal('tutorials', ['Tutorial'])


    def backwards(self, orm):
        
        # Deleting model 'Tutorial'
        db.delete_table('tutorials')


    models = {
        'tutorials.tutorial': {
            'Meta': {'object_name': 'Tutorial'},
            'duration': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'resume': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'thumbnail': ('filebrowser.fields.FileBrowseField', [], {'max_length': '200'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '150'}),
            'video': ('django.db.models.fields.URLField', [], {'max_length': '200'})
        }
    }

    complete_apps = ['tutorials']

from django.conf.urls.defaults import patterns, include, url
from django.conf import settings
from django.views.generic.simple import direct_to_template
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import oembed
oembed.autodiscover()

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Admin
    url(r'^admin/', include(admin.site.urls)),
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^admin/filebrowser/', include('filebrowser.urls')),

    # Tutorials
    url(r'^', include('src.tutorials.urls', namespace='tutorials')),

    # Categories
    url(r'^', include('src.categories.urls', namespace='categories')),

    # About
    url(r'^sobre/$', direct_to_template,
        {'template': 'about.html'}, name='about'),
)

if settings.DEBUG:
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve',
        {'document_root': settings.MEDIA_ROOT}),
    )

    urlpatterns += staticfiles_urlpatterns()
